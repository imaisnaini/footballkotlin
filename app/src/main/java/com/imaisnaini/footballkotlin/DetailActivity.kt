package com.imaisnaini.footballkotlin

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import org.jetbrains.anko.*

class DetailActivity : AppCompatActivity(), AnkoLogger {
    private var name: String = ""
    private var detail: String = ""
    private var icon: Int = 0
    lateinit var tvName: TextView
    lateinit var tvDetail: TextView
    lateinit var ivIcon: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initLayout()
        init()
    }

    private fun init(){
        val intent = intent
        name = intent.getStringExtra("name")
        icon = intent.getStringExtra("icon").toInt()
        detail = intent.getStringExtra("detail")

        tvName.text = name
        tvDetail.text = detail
        Glide.with(this).load(icon).into(ivIcon)
    }

    private fun initLayout(){
        linearLayout() {
            padding = dip(16)
            gravity = Gravity.CENTER_HORIZONTAL
            orientation = LinearLayout.VERTICAL

            ivIcon = imageView().lparams(width = matchParent, height = dip(150)) {
                rightMargin = dip(16)
            }

            tvName = textView() {
                textSize = 24f
                textColor = ContextCompat.getColor(context, R.color.colorPrimaryDark)
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                gravity = Gravity.CENTER_VERTICAL
            }.lparams(height = dip(80), width = matchParent)

            tvDetail = textView() {
                textSize = 20f
                textAlignment = View.TEXT_ALIGNMENT_INHERIT
            }.lparams(width = matchParent)
        }
    }
}
