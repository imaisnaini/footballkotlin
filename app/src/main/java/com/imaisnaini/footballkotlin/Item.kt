package com.imaisnaini.footballkotlin

/**
 * Created by hp on 06/09/2018.
 */
data class Item(val name : String?, val icon : Int?, val detail : String?)