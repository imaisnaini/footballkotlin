package com.imaisnaini.footballkotlin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity() {
    private var items : MutableList<Item> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val list = findViewById<RecyclerView>(R.id.activity_main_rvContent)
        activity_main_rvContent
        initData()

        list.layoutManager = LinearLayoutManager(this)
        list.adapter = RecyclerViewAdapter(this, items){
            startActivity<DetailActivity>(
                    "name" to it.name,
                    "icon" to it.icon.toString(),
                    "detail" to it.detail) // Anko Cosmos
        }
    }

    private fun initData(){
        val name = resources.getStringArray(R.array.club_name)
        val icon = resources.obtainTypedArray(R.array.club_image)
        val detail = resources.getStringArray(R.array.club_detail)
        items.clear()

        for (i in name.indices){
            items.add(Item(name[i], icon.getResourceId(i,0), detail[i]))
        }

        icon.recycle()
    }
}
