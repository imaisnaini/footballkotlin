package com.imaisnaini.footballkotlin

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_list.view.*

/**
 * Created by hp on 06/09/2018.
 */
class RecyclerViewAdapter(private val ctx : Context, private val items : List<Item>, private val listener : (Item) -> Unit)
    : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(){
    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(items[position], listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(ctx).inflate(R.layout.item_list, parent, false))

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view){

        fun bindItems(items: Item, listener: (Item) -> Unit){
            // Android KTX
            itemView.item_list_tvName.text = items.name
            Glide.with(itemView.context).load(items.icon).into(itemView.item_list_ivIcon)
            itemView.setOnClickListener{
                listener(items)
            }
        }
    }
}